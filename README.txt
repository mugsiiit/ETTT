Extreme Tic Tac Toe

This is a 16x16 board, with 16 sub-boards of size 4x4.
The heuristics used takes into account chances of winning at different positions, and potential positions of blocking the opponent.

The code is written in python2.

To run:
python simulator.py 1

Here the bot is "x", and opponent is "o"